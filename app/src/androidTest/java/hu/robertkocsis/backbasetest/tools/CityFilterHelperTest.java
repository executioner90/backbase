package hu.robertkocsis.backbasetest.tools;

import android.support.test.runner.AndroidJUnit4;
import hu.robertkocsis.backbasetest.pojo.City;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class CityFilterHelperTest {

    private CityFilterHelper cityFilterHelper;

    @Before
    public void setUp() throws Exception {
        cityFilterHelper = new CityFilterHelper();
    }

    @Test
    public void testNoPrefixUsed() throws Exception {
        List<City> mockedCitiesList = getMockedCitiesList();
        cityFilterHelper.setCities(mockedCitiesList);
        List<City> filteredCities = cityFilterHelper.performFiltering("");

        assertEquals(filteredCities.size(), mockedCitiesList.size());
    }

    @Test
    public void testAPrefixUsed() throws Exception {
        cityFilterHelper.setCities(getMockedCitiesList());
        List<City> filteredCities = cityFilterHelper.performFiltering("A");

        assertEquals(filteredCities.size(), 4);
    }

    @Test
    public void testSPrefixUsed() throws Exception {
        cityFilterHelper.setCities(getMockedCitiesList());
        List<City> filteredCities = cityFilterHelper.performFiltering("S");

        assertEquals(filteredCities.size(), 1);
    }

    @Test
    public void testNoFilterResult() throws Exception {
        cityFilterHelper.setCities(getMockedCitiesList());
        List<City> filteredCities = cityFilterHelper.performFiltering("C");

        assertEquals(filteredCities.size(), 0);
    }

    private List<City> getMockedCitiesList() {
        List<City> cities = new ArrayList<>();
        cities.add(new City("US", "Alabama", "Alabama", 0, 0));
        cities.add(new City("US", "Albuquerque", "Albuquerque", 0, 0));
        cities.add(new City("US", "Anaheim", "Anaheim", 0, 0));
        cities.add(new City("US", "Arizona", "Arizona", 0, 0));
        cities.add(new City("AU", "Sydney", "Sydney", 0, 0));
        return cities;
    }
}
