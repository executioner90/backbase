package hu.robertkocsis.backbasetest.tools;

import android.support.v4.util.ArrayMap;
import hu.robertkocsis.backbasetest.pojo.City;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CityFilterHelper {
    Map<String, List<City>> cities = new ArrayMap<>();
    List<City> unFilteredCities = new ArrayList<>();

    public void setCities(List<City> citiesArray) {
        unFilteredCities.addAll(citiesArray);
        for (City city : citiesArray) {
            String start = city.getNormalizedName().substring(0, 1).toLowerCase();
            List<City> alphabeticCities = cities.get(start);
            if (alphabeticCities == null) {
                alphabeticCities = new ArrayList<>();
            }
            alphabeticCities.add(city);
            cities.put(start, alphabeticCities);
        }
    }

    public List<City> performFiltering(CharSequence constraint) {
        List<City> filteredCities = new ArrayList<>();
        if (constraint.length() == 0) {
            filteredCities.addAll(unFilteredCities);
        } else {
            String filterPattern = constraint.toString().toLowerCase().trim();
            filterPattern = Normalizer.normalize(filterPattern, Normalizer.Form.NFKD)
                    .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

            List<City> cities = this.cities.get(filterPattern.substring(0, 1));
            if (cities != null && cities.size() > 0) {
                for (City city : cities) {
                    if (city.getNormalizedName().toLowerCase().startsWith(filterPattern)) {
                        filteredCities.add(city);
                    }
                }
            }
        }
        return filteredCities;
    }
}
