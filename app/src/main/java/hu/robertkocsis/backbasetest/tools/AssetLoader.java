package hu.robertkocsis.backbasetest.tools;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import hu.robertkocsis.backbasetest.dto.CityDto;
import hu.robertkocsis.backbasetest.pojo.City;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class AssetLoader {

    public List<City> loadCitiesJson(Context context) {
        BufferedReader reader = null;
        Gson gson = new Gson();
        List<City> cities = new ArrayList<>();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("cities.json")));
            CityDto[] citiesArray = gson.fromJson(reader, CityDto[].class);
            for (CityDto cityDto : citiesArray) {
                String normalizedName = Normalizer.normalize(cityDto.getName(), Normalizer.Form.NFKD);
                String asciiName = normalizedName.replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                        .replaceAll("'", "").replaceAll("‘", "")
                        .replaceAll("‘", "").trim();
                if (asciiName.length() == 0) {
                    asciiName = normalizedName;
                }
                cities.add(new City(
                        cityDto.getCountry(),
                        cityDto.getName(),
                        asciiName,
                        Double.parseDouble(cityDto.getCoord().getLon()),
                        Double.parseDouble(cityDto.getCoord().getLat())
                ));
            }
        } catch (IOException e) {
            Log.e("AssetLoader", "Failed to read json", e);
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return cities;
    }

}
