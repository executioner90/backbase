package hu.robertkocsis.backbasetest.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import hu.robertkocsis.backbasetest.R;
import hu.robertkocsis.backbasetest.pojo.City;
import hu.robertkocsis.backbasetest.tools.CityFilterHelper;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements Filterable,
        View.OnClickListener {
    private final static String TITLE_PATTERN = "{0} ({1})";

    // state
    private List<City> filteredCities = new ArrayList<>();
    private CustomFilter filter;
    private RecyclerViewClickListener recyclerViewClickListener;

    // dependency
    private CityFilterHelper cityFilterHelper;

    public RecyclerViewAdapter() {
        cityFilterHelper = new CityFilterHelper();
        filter = new CustomFilter();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        City city = filteredCities.get(position);
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);
        holder.titleText.setText(MessageFormat.format(TITLE_PATTERN, city.getName(), city.getCountry()));
    }

    @Override
    public int getItemCount() {
        return filteredCities.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void setRecyclerViewClickListener(RecyclerViewClickListener recyclerViewClickListener) {
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    public void setCities(List<City> cities) {
        filteredCities = cities;
        cityFilterHelper.setCities(cities);
    }

    // OnClickListener

    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        if (recyclerViewClickListener != null) {
            recyclerViewClickListener.onClick(view, pos, filteredCities.get(pos));
        }
    }

    // ViewHolder

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView titleText;

        ViewHolder(View view) {
            super(view);
            titleText = view.findViewById(R.id.title_text);
        }
    }

    // Filter

    public class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults results = new FilterResults();
            filteredCities = cityFilterHelper.performFiltering(constraint);
            results.values = filteredCities;
            results.count = filteredCities.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    }

    public interface RecyclerViewClickListener {

        void onClick(View view, int position, Object item);
    }
}
