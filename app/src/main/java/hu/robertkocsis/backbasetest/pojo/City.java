package hu.robertkocsis.backbasetest.pojo;

import java.io.Serializable;

public class City implements Serializable {
    public static String BUNDLE_KEY = "key_city";

    private String country;
    private String name;
    private String normalizedName;
    private double lon;
    private double lat;

    public City(String country, String name, String normalizedName, double lon, double lat) {
        this.country = country;
        this.name = name;
        this.normalizedName = normalizedName;
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public String getNormalizedName() {
        return normalizedName;
    }

    public String getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }
}
