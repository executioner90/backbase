package hu.robertkocsis.backbasetest.dto;

public class CityDto {
    String country;
    String name;
    CoordinateDto coord;

    public String getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }

    public CoordinateDto getCoord() {
        return coord;
    }
}
