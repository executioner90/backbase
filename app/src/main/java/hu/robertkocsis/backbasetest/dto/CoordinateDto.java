package hu.robertkocsis.backbasetest.dto;

public class CoordinateDto {
    String lon;
    String lat;

    public String getLon() {
        return lon;
    }

    public String getLat() {
        return lat;
    }
}
