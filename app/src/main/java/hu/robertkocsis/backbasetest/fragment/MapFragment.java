package hu.robertkocsis.backbasetest.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import hu.robertkocsis.backbasetest.R;
import hu.robertkocsis.backbasetest.pojo.City;

public class MapFragment extends Fragment {

    // UI
    MapView mapView;

    // factory
    public static MapFragment newInstance(@NonNull City city) {
        MapFragment fragment = new MapFragment();
        Bundle extras = new Bundle();
        extras.putSerializable(City.BUNDLE_KEY, city);
        fragment.setArguments(extras);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);

        mapView = view.findViewById(R.id.map_mapview);
        mapView.onCreate(savedInstanceState);

        final City city = (City) getArguments().getSerializable(City.BUNDLE_KEY);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng selectedCity = new LatLng(city.getLat(), city.getLon());
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(selectedCity));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
