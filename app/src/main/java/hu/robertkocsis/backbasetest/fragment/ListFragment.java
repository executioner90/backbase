package hu.robertkocsis.backbasetest.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import hu.robertkocsis.backbasetest.R;
import hu.robertkocsis.backbasetest.callback.ActivityCallback;
import hu.robertkocsis.backbasetest.pojo.City;
import hu.robertkocsis.backbasetest.tools.AssetLoader;
import hu.robertkocsis.backbasetest.ui.adapter.RecyclerViewAdapter;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class ListFragment extends Fragment {
    // UI
    ProgressBar progressBar;

    // state
    RecyclerViewAdapter adapter;
    ActivityCallback activityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityCallback = (ActivityCallback) context;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        adapter = new RecyclerViewAdapter();

        EditText searchText = view.findViewById(R.id.search_text);
        RecyclerView recyclerView = view.findViewById(R.id.cities_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        progressBar = view.findViewById(R.id.loading_indicator);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        adapter.setRecyclerViewClickListener(new RecyclerViewAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position, Object item) {
                if (activityCallback != null) {
                    activityCallback.onListItemSelected((City) item);
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CityLoader loader = new CityLoader(new Action() {
            @Override
            public void doAction(List<City> cities) {
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing() && !activity.isChangingConfigurations()) {
                    adapter.setCities(cities);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
        loader.execute(getContext());
    }

    static class CityLoader extends AsyncTask<Context, Void, List<City>> {
        // state
        Action action;

        // dependency
        AssetLoader loader = new AssetLoader();
        Collator collator = Collator.getInstance(Locale.UK);

        CityLoader(Action action) {
            this.action = action;
            collator.setStrength(Collator.PRIMARY);
        }

        @Override
        protected List<City> doInBackground(Context... voids) {
            final List<City> cities = loader.loadCitiesJson(voids[0]);
            // sort cities by name
            Collections.sort(cities, new Comparator<City>() {
                @Override
                public int compare(City city, City t1) {
                    return collator.compare(city.getName(), t1.getName());
                }
            });
            return cities;
        }

        @Override
        protected void onPostExecute(List<City> cities) {
            action.doAction(cities);
        }
    }

    interface Action {
        void doAction(List<City> cities);
    }
}
