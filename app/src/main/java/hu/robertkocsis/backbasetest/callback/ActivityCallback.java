package hu.robertkocsis.backbasetest.callback;

import hu.robertkocsis.backbasetest.pojo.City;

public interface ActivityCallback {
    void onListItemSelected(City city);
}
