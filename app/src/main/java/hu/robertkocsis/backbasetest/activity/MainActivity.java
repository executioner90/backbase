package hu.robertkocsis.backbasetest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import hu.robertkocsis.backbasetest.R;
import hu.robertkocsis.backbasetest.callback.ActivityCallback;
import hu.robertkocsis.backbasetest.fragment.ListFragment;
import hu.robertkocsis.backbasetest.fragment.MapFragment;
import hu.robertkocsis.backbasetest.pojo.City;

public class MainActivity extends AppCompatActivity implements ActivityCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_holder, new ListFragment(), "ListFrag")
                    .addToBackStack("List")
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    // ActivityCallback

    @Override
    public void onListItemSelected(City city) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_holder, MapFragment.newInstance(city), "MapFrag")
                .addToBackStack("Map")
                .commit();
    }

}
